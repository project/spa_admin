# Single-Page App Administration

The `spa_admin` project provides a bare theme allowing to integrate a
single-page app within Drupal's UI. It may only be activated on specific routes
or on all routes. Once the app is activated, that is embedded for the first
time, it will take over browser navigation.

On top of this, it also provides a route allowing to render the markup for
entity contextual links, to allow to integrate Drupal's administration into the
app UI.

**Note:** this depends on [#3267586-2: Make the addContextualLinks method
public](https://www.drupal.org/project/drupal/issues/3267586#comment-14432161).


## Installation

This assumes `composer` will be used to install the module and apply the patch.
You can find more information about alternative ways at:
- https://www.drupal.org/docs/extending-drupal/installing-modules
- https://www.drupal.org/docs/develop/git/using-git-to-contribute-to-drupal/applying-a-patch-in-a-feature-branch

Add the module to your project via the following command:

```
composer require drupal/spa_admin:dev-1.0.x
```

Add the following line to your `composer.json` or `composer.patches.json` file,
depending on how your `composer.json` file is structured (see [the related
documentation](https://www.drupal.org/docs/develop/git/using-git-to-contribute-to-drupal/applying-a-patch-in-a-feature-branch#s-composer)
for more details):

```
"Issue #3267586: Make the addContextualLinks method public": "https://www.drupal.org/files/issues/2022-03-03/entity-add-contextual-links-method-3267586.2.patch"
```

The end result should look more or less like this:

```
{
  "patches": {
    "drupal/core": {
      ...
      "Issue #3267586: Make the addContextualLinks method public": "https://www.drupal.org/files/issues/2022-03-03/entity-add-contextual-links-method-3267586.2.patch"
    },
    ...
  }
}
```

Run `composer install` to apply the patch.

The `spa_admin` theme includes a `spa_admin_helper` module that is required to
make it work. Enable the theme and enable the module as usual:

* https://www.drupal.org/docs/extending-drupal/installing-themes
* https://www.drupal.org/docs/extending-drupal/installing-modules#s-step-2-enable-the-module


## Sub-themes

You can also create a `spa_admin` sub-theme and enable that as well, in which
case you need to configure it as the SPA-administration theme, via config or PHP
settings (no UI exists for this yet):

* Update the config by either manually change `spa_admin_helper.settings.yml`
  and run a config import or via drush:
  ```
  drush cset -y spa_admin_helper.settings theme spa_admin_subtheme_name
  ```
* Override config via PHP settings:
  ```php
  $config['spa_admin_helper.settings']['theme'] = 'spa_admin_subtheme_name';
  ```


## Usage

First you need to tell the system how to embed the SPA: you do this by selecting
an **embed provider**. Embed providers are plug-ins implementing the logic
required to generate the markup that will embed the SPA. They are also
responsible for providing the library info needed to actually include the app
Javascript/CSS assets.

The only embed provider `spa_admin` implements natively is `react_local`, which
will embed a React (or Preact) app available in the current project codebase. By
default it will look for it at `/path/to/drupal/app/app`, but this is obviously
configurable.

Again, no administrative UI is currently available, so you need to configure the
embed provider via config or PHP settings. Here's an example with the latter:

```php
$config['spa_admin_helper.settings']['provider'] = 'react_local';
$config['spa_admin_helper.providers']['react_local']['app_id'] = 'my-app';
```

This will generate the following markup and import all the React app assets:

```html
<div id="my-app"></div>
```

**Note:** the asset paths are read directly from the app manifest and normally
change whenever the app is built, so a cache clear is required to refresh the
library info accordingly.

As a second step, you need to configure the **embed URLs**. By default the
system will only try to embed the app when the `/spa-admin/app` path is
visited. You can configure this behavior by changing the following config item:

```php
$config['spa_admin_helper.settings']['embed_url_pattern'] = '.*';
```

This will embed the app on any non-administrative Drupal route. Otherwise you
can mix the app with the regular site UI, through a more specific configuration,
for instance:

```php
$config['spa_admin_helper.settings']['embed_url_pattern'] = '/home|/news|/about|/article/.*|node/.*/layout';
```


## Contextual links

Embedding contextual links is rather straightforward: if an SPA is being
embedded via `spa_admin`, any element having a `data-decoupled-contextual`
attribute will trigger the embed of a contextual link dropdown. For instance,
the markup following markup triggers the embed of contextual links for node `1`
appended to the element itself:

```html
<section data-decoupled-contextual="entity/node/1">
...
</section>
```

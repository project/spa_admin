(function (Drupal) {
  'use strict';
  Drupal.behaviors.ginOverride = {
    attach: function (context, settings) {
      const newParent = document.querySelector('.region-sticky__items__inner');
      if (!newParent) {
        return;
      }

      // Move the add template button to the sticky region.
      const button = context.querySelector('.layout-builder .layout-builder__link--add-template-to-library');
      if (button) {
        button.classList.add('button');
        button.classList.add('button--primary');
        button.textContent = Drupal.t('Add to library');
        const existingButton = newParent.querySelector('.layout-builder__link--add-template-to-library');
        if (existingButton) {
          existingButton.remove();
        }
        newParent.append(button);
      }

      once('lb-plus-admin', 'form.layout-builder-form', context).forEach((form) => {
        // Move the Layout builder form to the sticky region.
        newParent.appendChild(form);

        // Create the dropdown button.
        const dropdownButton = document.createElement('div');
        dropdownButton.classList.add('lb-plus-layout-actions');
        const html = `
          <div class="dropbutton-wrapper">
            <div class="dropbutton-widget">
              <ul class="dropbutton dropbutton--multiple dropbutton--gin">
                <li id="dropbutton-default-action" class="dropbutton__item"></li>
                <li class="dropbutton__item">
                  <ul class="dropbutton__items"></ul>
                </li>
              </ul>
            </div>
          </div>
        `;
        dropdownButton.innerHTML = html;

        // Populate the dropdown button with the Lsyout builder form button.
        form.querySelectorAll('input[type="submit"]').forEach((button, index) => {
          if (index === 0) {
            dropdownButton.querySelector('#dropbutton-default-action').appendChild(button);
          }
          else {
            const li = document.createElement('li');
            li.classList.add('dropbutton__item');
            li.appendChild(button);
            dropdownButton.querySelector('.dropbutton__items').appendChild(li);
          }
        });

        // Append the dropdown button to the original form.
        Drupal.behaviors.dropButton.attach(dropdownButton, settings);
        form.querySelector('.form-actions').prepend(dropdownButton);

        // Re-attach the toggle preview behaviour.
        Drupal.behaviors.layoutBuilderToggleContentPreview.attach();
      });
    }
  }
})(Drupal);

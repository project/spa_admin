<?php

namespace Drupal\spa_admin_helper\PageCache\ResponsePolicy;

use Drupal\Core\PageCache\ResponsePolicyInterface;
use Drupal\spa_admin_helper\Handler\EmbedHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Dynamic page cache response policy.
 */
class DenySpaAdminRoutes implements ResponsePolicyInterface {

  /**
   * The embed handler.
   *
   * @var \Drupal\spa_admin_helper\Handler\EmbedHandler
   */
  protected EmbedHandler $embedHandler;

  /**
   * SpaEmbedController constructor.
   */
  public function __construct(EmbedHandler $embed_handler) {
    $this->embedHandler = $embed_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function check(Response $response, Request $request) {
    // We dynamically negotiate the current theme, but dynamic page cache skips
    // this, so we do not support it.
    return $this->embedHandler->isEmbedding($request) ? static::DENY : NULL;
  }

}

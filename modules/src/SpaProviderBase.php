<?php

namespace Drupal\spa_admin_helper;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base class for SPA providers.
 */
abstract class SpaProviderBase extends PluginBase implements SpaProviderInterface {

  /**
   * Returns the app identifier.
   *
   * @return string
   *   An arbitrary identifier.
   */
  protected function getAppId(): string {
    return $this->configuration['app_id'] ?? 'app';
  }

  /**
   * Returns the path where the app is installed relative to the Drupal root.
   *
   * @return string
   *  A relative path.
   */
  protected function getAppPath(): string {
    return $this->configuration['app_path'] ?? '/app/' . $this->getAppId();
  }

  /**
   * Returns the ID of the library used to embed the app.
   *
   * @return string
   *   An arbitrary library identifier.
   */
  protected function getLibraryId(): string {
    return strtr($this->getAppId(), '-', '_');
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraryInfo(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildSpaEmbed(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildDrupalUIEmbed(Request $request, callable $controller, array $arguments) {
    return call_user_func_array($controller, $arguments);
  }


  /**
   * {@inheritdoc}
   */
  public function getBlockSuggestions(array &$suggestions) {
    $suggestions[] = 'block__static_component';
  }

  /**
   * {@inheritdoc}
   */
  public function addPreprocessVariables(EntityInterface $entity, array &$variables) {
    $variables['label'] = $entity->label();
  }

}

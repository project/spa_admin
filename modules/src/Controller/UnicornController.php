<?php

namespace Drupal\spa_admin_helper\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatch;
use Drupal\spa_admin_helper\Handler\EmbedHandler;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Pseudo-controller allowing to embed an SPA on any user-facing route.
 */
class UnicornController implements EventSubscriberInterface {

  /**
   * The HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * The even dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The route admin context to determine whether a route is an admin one.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected AdminContext $adminContext;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The embed handler.
   *
   * @var \Drupal\spa_admin_helper\Handler\EmbedHandler
   */
  protected EmbedHandler $embedHandler;

  /**
   * FrontController constructor.
   */
  public function __construct(
    HttpKernelInterface $http_kernel,
    EventDispatcherInterface $event_dispatcher,
    AdminContext $admin_context,
    ConfigFactoryInterface $config_factory,
    EmbedHandler $embed_handler,
    ArgumentResolverInterface $argument_resolver
  ) {
    $this->httpKernel = $http_kernel;
    $this->eventDispatcher = $event_dispatcher;
    $this->adminContext = $admin_context;
    $this->configFactory = $config_factory;
    $this->embedHandler = $embed_handler;
    $this->argumentResolver = $argument_resolver;
  }

  /**
   * Acts on the "Request" event.
   */
  public function onRequest(RequestEvent $event): void {
    if (PHP_SAPI === 'cli') {
      return;
    }

    // Skip any request not trying to retrieve an HTML page.
    $request = $event->getRequest();
    if ($request->getMethod() !== 'GET' || $request->getRequestFormat() !== 'html' || $request->query->has(MainContentViewSubscriber::WRAPPER_FORMAT)) {
      return;
    }
    if (!array_intersect(['text/html', 'application/xhtml+xml'], $request->getAcceptableContentTypes())) {
      return;
    }

    // Skip exceptions.
    $skip_routes = $this->configFactory->get('spa_admin_helper.settings')->get('skip_routes') ?? [];
    $route_match = RouteMatch::createFromRequest($request);
    $route_name = $route_match->getRouteName();
    if (in_array($route_name, $skip_routes) || str_starts_with($route_name, 'spa_admin_helper.')) {
      return;
    }

    // Skip excluded paths.
    $route = $route_match->getRouteObject();
    $pattern = $this->configFactory->get('spa_admin_helper.settings')->get('embed_url_pattern');
    $path = $route->getPath();

    $not_found = FALSE;
    // If the specified route is not defined, it may be defined in the app, so
    // check it anyway.
    if ($path === '/system/404') {
      $path = $request->getRequestUri();
      $not_found = TRUE;
    }

    if (!preg_match('%^' . $pattern . '$%', $path) || $this->adminContext->isAdminRoute($route)) {
      return;
    }

    $this->embedHandler->setEmbedding($request);

    // @todo: Check for if we should render the main content here or not.
    if (!$not_found) {
      return;
    }

    $build = $this->embedHandler->buildSpaEmbed();

    // Render the response as a regular HTML response.
    $view_event = new ViewEvent($this->httpKernel, $request, $event->getRequestType(), $build);
    $this->eventDispatcher->dispatch($view_event, KernelEvents::VIEW);
    if ($view_event->hasResponse()) {
      $event->setResponse($view_event->getResponse());
    }
  }

  /**
   * Acts on the "Response" event.
   */
  public function onResponse(ResponseEvent $event): void {
    // Add support for undefined routes as-well.
    $response = $event->getResponse();
    if ($response->getStatusCode() === Response::HTTP_NOT_FOUND && $this->embedHandler->isEmbedding($event->getRequest())) {
      $response->setStatusCode(Response::HTTP_OK);
    }
  }

  /**
   * Puts the response of the controller in drupalSettings.
   *
   * @param \Symfony\Component\HttpKernel\Event\ControllerEvent $event
   *   The controller event.
   */
  public function onController(ControllerEvent $event) {
    $request = $event->getRequest();
    $controller = $event->getController();

    if (!$this->embedHandler->isEmbedding($request)) {
      return;
    }

    // See \Symfony\Component\HttpKernel\HttpKernel::handleRaw().
    $arguments = $this->argumentResolver->getArguments($request, $controller);

    $event->setController(function () use ($controller, $arguments, $event) {
      return $this->wrapControllerExecution($controller, $arguments, $event);
    });
  }

  /**
   * Wraps a controller execution for storing in drupalSettings.
   *
   * @param callable $controller
   *   The controller to execute.
   * @param array $arguments
   *   The arguments to pass to the controller.
   * @param \Symfony\Component\HttpKernel\Event\ControllerEvent $event
   *   The controller event.
   *
   * @return mixed
   *   The return value of the controller.
   *
   * @see \Symfony\Component\HttpKernel\HttpKernel::handleRaw()
   */
  protected function wrapControllerExecution($controller, array $arguments, ControllerEvent $event) {
    $request = $event->getRequest();
    $response = $this->embedHandler->buildDrupalUIEmbed($request, $controller, $arguments);

    if (!is_array($response)) {
      return $response;
    }

    $build = [
      'drupal' => $response,
      'spa' => $this->embedHandler->buildSpaEmbed(),
    ];

    // Render the response as a regular HTML response.
    $view_event = new ViewEvent($this->httpKernel, $request, $event->getRequestType(), $build);
    $this->eventDispatcher->dispatch($view_event, KernelEvents::VIEW);
    if ($view_event->hasResponse()) {
      return $view_event->getResponse();
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => [['onRequest']],
      KernelEvents::RESPONSE => [['onResponse']],
      KernelEvents::CONTROLLER => [['onController']],
    ];
  }

}

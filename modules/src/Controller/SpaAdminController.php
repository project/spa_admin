<?php

namespace Drupal\spa_admin_helper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * The SPA administration controller.
 */
class SpaAdminController extends ControllerBase {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * SpaAdminController constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer) {
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Renders contextual links for the specified entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity whose links should be rendered.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The controller response.
   *
   * @throws \Exception
   */
  public function getEntityContextualLinks(Request $request, EntityInterface $entity) {
    $entity_type_id = $entity->getEntityTypeId();
    // Pretend we are dealing with a default revision, otherwise contextual
    // links will not be generated.
    if ($entity instanceof RevisionableInterface) {
      $entity->isDefaultRevision(TRUE);
    }

    $build = [];
    $view_builder = $this->entityTypeManager->getViewBuilder($entity_type_id);
    $view_builder->addContextualLinks($build, $entity);
    if (!empty($build['#contextual_links'])) {
      $build['#type'] = 'contextual_links';
      foreach ($build['#contextual_links'] as &$contextual_link) {
        $contextual_link['metadata']['ajax_destination'] = $request->query->get('destination');
      }
    }

    $output = $this->renderer->render($build);
    return new Response($output);
  }

}

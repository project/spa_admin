<?php

namespace Drupal\spa_admin_helper\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\spa_admin_helper\Handler\EmbedHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The "SPA Admin" embed controller.
 */
class SpaEmbedController implements ContainerInjectionInterface {

  /**
   * The embed handler.
   *
   * @var \Drupal\spa_admin_helper\Handler\EmbedHandler
   */
  protected EmbedHandler $embedHandler;

  /**
   * SpaEmbedController constructor.
   */
  public function __construct(EmbedHandler $embed_handler) {
    $this->embedHandler = $embed_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('spa_admin_helper.embed_handler'));
  }

  /**
   * Embeds the app, if needed.
   */
  public function embed(Request $request): array {
    $this->embedHandler->setEmbedding($request);
    return $this->embedHandler->buildSpaEmbed() ?: [];
  }

}

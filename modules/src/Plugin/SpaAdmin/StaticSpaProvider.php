<?php

namespace Drupal\spa_admin_helper\Plugin\SpaAdmin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\spa_admin_helper\SpaProviderBase;
use http\Client\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Static SPA provider.
 *
 * @Plugin(
 *   id = "static",
 *   label = @Translation("Static"),
 * )
 */
class StaticSpaProvider extends SpaProviderBase {
  // Use the parent class methods.
}

<?php

namespace Drupal\spa_admin_helper\Plugin\SpaAdmin;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\spa_admin_helper\SpaProviderBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Local React SPA provider.
 *
 * @Plugin(
 *   id = "react_local",
 *   label = @Translation("React - Local"),
 * )
 */
class ReactSpaProvider extends SpaProviderBase {

  /**
   * {@inheritdoc}
   */
  public function getLibraryInfo(): array {
    $info = [];

    $app_path = trim($this->getAppPath(), '/');
    $asset_path = $app_path . '/build';
    $manifest_path = DRUPAL_ROOT . '/' . $asset_path . '/asset-manifest.json';

    if (file_exists($manifest_path)) {
      $content = file_get_contents($manifest_path);
      $json = Json::decode($content);
      $library_id = $this->getLibraryId();

      foreach ($json['entrypoints'] ?? [] as $entrypoint) {
        $ext = pathinfo($entrypoint, PATHINFO_EXTENSION);
        $asset = Url::fromUri('base://' . $asset_path . '/' . $entrypoint)
          ->toString(TRUE)
          ->getGeneratedUrl();

        if ($ext === 'js') {
          $info[$library_id]['js'] = [$asset => ['minified' => FALSE, 'attributes' => ['data-app' => TRUE]]];
        }
        elseif ($ext === 'css') {
          $info[$library_id]['css']['base'] = [$asset => ['minified' => FALSE, 'attributes' => ['data-app' => TRUE]]];
        }
      }
    }

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function buildSpaEmbed(): array {
    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => '',
      '#attributes' => [
        'id' => $this->getAppId(),
      ],
      '#attached' => [
        'library' => ['spa_admin_helper/' . $this->getLibraryId()],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildDrupalUIEmbed(Request $request, callable $controller, array $arguments) {
    $build = [];

    $response_build = call_user_func_array($controller, $arguments);
    if (!is_array($response_build)) {
      return $response_build;
    }

    $build['response'] = [
      '#type' => 'html_tag',
      '#tag' => 'template',
      '#attributes' => ['id' => 'spa-admin-response'],
    ];

    $build['response']['body'] = $response_build;

    $build['#attached']['library'][] = 'spa_admin_helper/injector';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockSuggestions(array &$suggestions) {
    $suggestions[] = 'block__decoupled_component';
  }

  /**
   * {@inheritdoc}
   */
  public function addPreprocessVariables(EntityInterface $entity, array &$variables) {
    $variables['type'] = sprintf('%s--%s', $entity->getEntityTypeId(), $entity->bundle());
    $variables['id'] = $entity->uuid();
    $variables['provider'] = 'jsonapi';

    $route_name = sprintf('jsonapi.%s.individual', $variables['type']);
    $variables['link'] = Url::fromRoute($route_name, ['entity' => $variables['id']])
      ->toString();

    $variables['use_shadowdom'] = !empty($this->configuration['use_shadowdom']);
  }

}

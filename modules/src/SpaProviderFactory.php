<?php

namespace Drupal\spa_admin_helper;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\spa_admin_helper\Plugin\SpaProviderPluginManager;

/**
 * The SPA provider factory.
 */
class SpaProviderFactory {

  /**
   * The SPA provider bucket plugin manager.
   *
   * @var \Drupal\spa_admin_helper\Plugin\SpaProviderPluginManager
   */
  protected SpaProviderPluginManager $pluginManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * SpaProviderFactory constructor.
   */
  public function __construct(
    SpaProviderPluginManager $plugin_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->pluginManager = $plugin_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * Returns the active provider.
   *
   * @return \Drupal\spa_admin_helper\SpaProviderInterface
   *   A SPA provider instance.
   */
  public function getActiveProvider(): SpaProviderInterface {
    $config = $this->configFactory->get('spa_admin_helper.settings');
    $provider_id = $config->get('provider');
    return $this->getProvider($provider_id);
  }

  /**
   * Returns the specified provider.
   *
   * @param string $provider_id
   *   The provider ID.
   *
   * @return \Drupal\spa_admin_helper\SpaProviderInterface
   *   A SPA provider instance.
   */
  public function getProvider(string $provider_id): SpaProviderInterface {
    try {
      $configuration = $this->configFactory->get('spa_admin_helper.providers')->get($provider_id);
      $provider = $this->pluginManager->createInstance($provider_id, $configuration ?: []);
      if ($provider instanceof SpaProviderInterface) {
        return $provider;
      }
    }
    catch (PluginException $e) {
    }
    throw new \LogicException('No valid SPA Provider found.');
  }

  /**
   * Returns a list of SPA providers.
   *
   * @return string[]
   *   An associative array of labels keyed by ID.
   */
  public function getList(): array {
    $list = [];
    foreach ($this->pluginManager->getDefinitions() as $id => $definition) {
      $list[$id] = $definition['label'] ?? $id;
    }
    asort($list);
    return $list;
  }

}

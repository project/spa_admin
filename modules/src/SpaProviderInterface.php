<?php

namespace Drupal\spa_admin_helper;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Common interface for SPA providers.
 */
interface SpaProviderInterface {

  /**
   * Returns the information for the library used to embed the app.
   *
   * @return array
   *   A library info array as expected by hook_library_info_build().
   *
   * @see hook_library_info_build()
   */
  public function getLibraryInfo(): array;

  /**
   * Builds the app render array.
   *
   * @return array
   *   A render array.
   */
  public function buildSpaEmbed(): array;

  /**
   * Builds the Drupal UI elements to be embedded in the app.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param callable $controller
   *   The controller generating the Drupal UI render array.
   * @param array $arguments
   *   The controller arguments.
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *   The actual controller response.
   */
  public function buildDrupalUIEmbed(Request $request, callable $controller, array $arguments);

  /**
   * Get the block suggestions.
   *
   * @return array
   *   The provider suggestions.
   */
  public function getBlockSuggestions(array &$suggestions);

  /**
   * Get the variables used in the suggested template.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The provider suggestions.
   *
   * @return array
   */
  public function addPreprocessVariables(EntityInterface $entity, array &$variables);
}

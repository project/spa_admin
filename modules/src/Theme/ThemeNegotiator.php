<?php

namespace Drupal\spa_admin_helper\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\spa_admin_helper\Handler\EmbedHandler;

/**
 * The "SPA Admin" theme negotiator.
 */
class ThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The negotiator configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The embed handler.
   *
   * @var \Drupal\spa_admin_helper\Handler\EmbedHandler
   */
  protected EmbedHandler $embedHandler;

  /**
   * ThemeNegotiator constructor.
   */
  public function __construct(
    RequestStack $request_stack,
    ConfigFactoryInterface $config_factory,
    EmbedHandler $embed_handler
  ) {
    $this->requestStack = $request_stack;
    $this->configFactory = $config_factory;
    $this->embedHandler = $embed_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $request = $this->requestStack->getCurrentRequest();
    return $this->embedHandler->isEmbedding($request);
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->configFactory
      ->get('spa_admin_helper.settings')
      ->get('theme');
  }

}

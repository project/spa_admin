<?php

namespace Drupal\spa_admin_helper\Handler;

use Drupal\Core\Entity\EntityInterface;
use Drupal\spa_admin_helper\SpaProviderFactory;
use Drupal\spa_admin_helper\SpaProviderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The "SPA Admin" embed handler.
 */
class EmbedHandler {

  /**
   * The SPA provider factory.
   *
   * @var \Drupal\spa_admin_helper\SpaProviderFactory
   */
  protected SpaProviderFactory $spaProviderFactory;

  /**
   * The current provider.
   *
   * @var ?\Drupal\spa_admin_helper\SpaProviderInterface
   */
  protected ?SpaProviderInterface $activeProvider = NULL;

  /**
   * Tracks SPA-embedding requests.
   *
   * @var string[]
   */
  protected array $embedding;

  /**
   * EmbedHandler constructor.
   */
  public function __construct(SpaProviderFactory $spa_provider_factory) {
    $this->spaProviderFactory = $spa_provider_factory;
  }

  public function getActiveProvider() {
    if (!$this->activeProvider instanceof SpaProviderInterface) {
      $this->activeProvider = $this->spaProviderFactory->getActiveProvider();
    }
    return $this->activeProvider;
  }

  /**
   * Checks whether the specified request should embed the SPA.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return bool
   *   TRUE if the SPA should be embedded, FALSE otherwise.
   */
  public function isEmbedding(Request $request): bool {
    return $request->attributes->get('spa_admin_embed') || !empty($this->embedding[$this->getRequestHash($request)]);
  }

  /**
   * Specifies whether the specified request should embed the SPA.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param bool $embedding
   *   TRUE if the SPA should be embedded, FALSE otherwise.
   */
  public function setEmbedding(Request $request, bool $embedding = TRUE): void {
    $this->embedding[$this->getRequestHash($request)] = TRUE;
    $request->attributes->set('spa_admin_embed', $embedding);
  }

  /**
   * Computes a request hash.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   An HTTP request.
   *
   * @return string
   *   A hash string.
   */
  protected function getRequestHash(Request $request): string {
    return hash('sha256', preg_replace('/\s+$/', '', $request->__toString()));
  }

  /**
   * Builds the SPA embed.
   *
   * @return array
   *   A render array.
   */
  public function buildSpaEmbed(): array {
    $provider = $this->getActiveProvider();
    return $provider->buildSpaEmbed();
  }

  /**
   * {@inheritdoc}
   */
  public function buildDrupalUIEmbed(Request $request, callable $controller, array $arguments) {
    $provider = $this->getActiveProvider();
    return $provider->buildDrupalUIEmbed($request, $controller, $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockSuggestions(array &$suggestions) {
    $provider = $this->getActiveProvider();
    return $provider->getBlockSuggestions($suggestions);
  }

  /**
   * {@inheritdoc}
   */
  public function addPreprocessVariables(EntityInterface $entity, array &$variables) {
    $provider = $this->getActiveProvider();
    return $provider->addPreprocessVariables($entity, $variables);
  }
}

<?php

namespace Drupal\spa_admin_helper\EventSubscriber;;

use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Ajax\OpenOffCanvasDialogCommand;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class SpaAdminHelperEventSubscriber implements EventSubscriberInterface {
  use AjaxHelperTrait;

  /**
   * The cache backend that should be used.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  public function __construct(CacheBackendInterface $cache, RouteMatchInterface $routeMatch, ConfigFactoryInterface $configFactory) {
    $this->cache = $cache;
    $this->routeMatch = $routeMatch;
    $this->configFactory = $configFactory;
  }

  /**
   * Rebuilds the router when the default or admin theme is changed.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The configuration event.
   */
  public function onConfigSave(ConfigCrudEvent $event) {
    $saved_config = $event->getConfig();
    if ($saved_config->getName() == 'system.theme') {
      $this->cache->invalidateAll();
    }
  }

  /**
   * Acts on the "Request" event.
   * Makes the off_canvas tray bigger.
   */
  public function onRequest(RequestEvent $event): void {
    if ($this->getRequestWrapperFormat() === 'drupal_dialog.off_canvas') {
      $width = $this->configFactory->get('spa_admin_helper.settings')->get('off_canvas_width') ?? OpenOffCanvasDialogCommand::DEFAULT_DIALOG_WIDTH;
      $request = $event->getRequest();
      $dialog_options = $request->request->get('dialogOptions') ?: [];
      $dialog_options['width'] = $width;
      $request->request->set('dialogOptions', $dialog_options);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE][] = ['onConfigSave', 0];
    $events[KernelEvents::REQUEST][] = ['onRequest', 0];
    return $events;
  }
}

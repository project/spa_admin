(function (Drupal) {

  Drupal.behaviors.spaAdminInjector = {

    attach: function () {
      const spaAdminElem = document.getElementById('spa-admin');
      const spaAdminResponse = document.getElementById('spa-admin-response');
      if (spaAdminElem && spaAdminResponse) {
        // Attach the behaviour once.
        once('spaAdminInjector-init', 'body').forEach(function () {
          spaAdminElem.innerHTML = spaAdminResponse.innerHTML;
          Drupal.attachBehaviors(spaAdminElem);
        });
      }
      else {
        window.setTimeout(Drupal.behaviors.spaAdminInjector.attach, 0);
      }

      /**
       * ShadowDecoupledElement custom element.
       */
      class ShadowDecoupledElement extends HTMLElement {
        constructor() {
          super();
          this.template = this.querySelector('template');
          this.shadow = this.attachShadow({mode: 'closed'});
          this.css = document.querySelectorAll('link[data-app]');
          this.js = document.querySelectorAll('script[data-app]');
        }
        connectedCallback() {
          this.shadow.appendChild(this.template.content.cloneNode(true));
          this.js.forEach(js => {
            this.shadow.appendChild(js.cloneNode(true));
          });
          this.css.forEach(css => {
            this.shadow.appendChild(css.cloneNode(true));
          });
        }
      }

      /**
       * Attach decoupled elements.
       */
      once('spa-admin-shadow', 'body').forEach(() => {
        customElements.define('spa-admin-decoupled-component', ShadowDecoupledElement);
      });
    },

  };

})(Drupal);

(function (Drupal) {
  Drupal.behaviors.spaAdminHelper = {

    attach: function (context, settings) {
      // Attach the behaviour once.
      once('spaAdminHelper-init', 'body').forEach(function (container) {

        const changeWorkpaceSwitcherAction = (path) => {
          const forms = document.querySelectorAll('[data-drupal-selector="workspace-switcher-form"], [data-drupal-selector="wse-workspace-switcher-form"]');
          if (forms.length > 0) {
            forms.forEach(form => form.action = '/admin?destination=' + encodeURI(path));
          }
        };

        // Workaround the problem that SPAs are often mounted at 404 paths.
        changeWorkpaceSwitcherAction(document.location.pathname);

        // Dispatch an event when app location changes.
        let pathname = document.location.pathname;
        const locationObserver = new MutationObserver((mutations) => {
          mutations.every((mutation) => {
            if (pathname !== document.location.pathname) {
              pathname = document.location.pathname;
              const locationEvent = new CustomEvent('spa_admin.locationChanged', {'detail': {'location': document.location}});
              document.dispatchEvent(locationEvent);
              return true;
            }
          });
        });

        locationObserver.observe(container, {childList: true, subtree: true});

        // Change the workspace switcher form action when the app location changes.
        document.addEventListener('spa_admin.locationChanged', (event) => {
          changeWorkpaceSwitcherAction(event.detail.location.pathname);
        });

        // Attach the contextual links on load.
        const nodes = document.querySelectorAll('[data-decoupled-contextual]');
        if (nodes.length > 0) {
          nodes.forEach((node) => {
            Drupal.behaviors.spaAdminHelper.setContextualLinks(node);
          });
        }

        const observer = new MutationObserver((mutations) => {
          const contextualLinks = [];

          mutations
            // Get all added nodes.
            .reduce((addedNodes, mutationRecord) => {
              return [...addedNodes, ...(Array.from(mutationRecord.addedNodes))];
            }, [])
            // Exclude text, attributes etc.
            .filter(node => node.nodeType === Node.ELEMENT_NODE)
            .forEach(node => {
              // Add the node itself with contextual links.
              if (node.dataset.hasOwnProperty('decoupledContextual')) {
                contextualLinks.push(node);
              }
              // Add any children with contextual links.
              node.querySelectorAll('[data-decoupled-contextual]').forEach(element => contextualLinks.push(element))
            });

          if (contextualLinks.length > 0) {
            contextualLinks.forEach((node) => {
              Drupal.behaviors.spaAdminHelper.setContextualLinks(node);
            });
          }
        });

        const config = {attributes: false, childList: true, subtree: true};
        observer.observe(document.body, config);
      });
    },

    isDescendant: function(tag, child) {
      let node = child.parentNode;
      while (node) {
        node = node.parentNode;
        if (node && node.tagName === tag.toUpperCase()) {
          return true;
        }
      }

      return false;
    },

    setContextualLinks: function(node) {
      // Add the contextual links only if the node is not a decoupled component.
      const isDecoupleComponent = Drupal.behaviors.spaAdminHelper.isDescendant('x-decoupled-component', node);
      if (!node.classList.contains('spa-admin-processed') && !isDecoupleComponent) {
        const contexualLink = node.dataset.decoupledContextual;
        const params = contexualLink.split('/')[1];
        const [entity_type, entity_id] = params.split(':');
        const destination = window.location.pathname + window.location.search;

        if (entity_type !== 'undefined' && Number.isInteger(parseInt(entity_id))) {
          fetch(`${drupalSettings.path.baseUrl}spa-admin/contextual/${entity_type}/${entity_id}?destination=${destination}`)
            .then(response => {
              if (response.status === 200) {
                return response.text();
              } else {
                throw response.status;
              }
            })
            .then(value => {
              const contextualLinks = document.createElement('div');
              contextualLinks.classList.add('spa-admin-contextual-links');
              contextualLinks.innerHTML = value;
              node.appendChild(contextualLinks);
              // Attach the contextual links once.
              node.classList.add('spa-admin-processed');
              // Re-attach the Ajax/Modal behaviours.
              Drupal.attachBehaviors(contextualLinks);
            });
        }
      }
    }

  };

})(Drupal);

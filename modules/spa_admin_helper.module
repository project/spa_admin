<?php

use Drupal\Component\Serialization\Json;
use Drupal\spa_admin_helper\SpaProviderFactory;
use Drupal\spa_admin_helper\Plugin\SpaAdmin\StaticSpaProvider;

/**
 * Implements hook_library_info_build().
 */
function spa_admin_helper_library_info_build() {
  $libraries = [];

  $factory = \Drupal::service('spa_admin_helper.spa_provider_factory');
  assert($factory instanceof SpaProviderFactory);
  foreach ($factory->getList() as $provider_id => $label) {
    $provider = $factory->getProvider($provider_id);
    $libraries += $provider->getLibraryInfo();
  }

  return $libraries;
}

/**
 * Implements hook_contextual_links_alter().
 */
function spa_admin_helper_contextual_links_alter(array &$links, $group, array $route_parameters) {
  foreach ($links as &$link) {
    if (isset($link['metadata']['ajax_destination'])) {
      $link['localized_options']['query'][] = ['destination' => $link['metadata']['ajax_destination']];
      $link['localized_options']['attributes']['class'][] = 'use-ajax';
      $link['localized_options']['attributes']['data-dialog-type'] = 'dialog';
      $link['localized_options']['attributes']['data-dialog-renderer'] = 'off_canvas';
      $dialog_options = !empty($link['localized_options']['attributes']['data-dialog-options']) ? Json::decode($link['localized_options']['attributes']['data-dialog-options']) : [];
      $dialog_options['classes'] = ['ui-dialog' => 'spa-admin-dialog'];
      $link['localized_options']['attributes']['data-dialog-options'] = Json::encode($dialog_options);
      unset($link['metadata']['ajax_destination']);
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function spa_admin_helper_page_attachments(array &$attachments) {
  if (\Drupal::currentUser()->hasPermission('access contextual links')) {
    $attachments['#attached']['library'][] = 'spa_admin_helper/admin';
  }

  // Attach the override library if is defined for current admin theme.
  $admin_theme = \Drupal::config('system.theme')->get('admin');
  $library =  \Drupal::service('library.discovery')->getLibraryByName('spa_admin', $admin_theme);
  if (!empty($library)) {
    $attachments['#attached']['library'][] = 'spa_admin/' . $admin_theme;
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function spa_admin_helper_preprocess_html(&$variables) {
  $page_title = (string) ($variables['head_title']['title'] ?? '');
  if ($page_title === (string) t('Page not found') && \Drupal::service('spa_admin_helper.embed_handler')->isEmbedding(\Drupal::request())) {
    $variables['head_title']['title'] = t('App Embed');
  }
}

/**
 * Implements hook_system_info_alter().
 */
function spa_admin_helper_system_info_alter(array &$info, \Drupal\Core\Extension\Extension $file, $type) {
  $override = \Drupal::configFactory()->get('spa_admin_helper.settings')->get('override_subtheme') ?? FALSE;
  if ($override && $file->getName() === 'spa_admin') {
    $admin_theme = \Drupal::config('system.theme')->get('admin');
    $info['base theme'] = $admin_theme;
  }
}

/**
 * Implements hook_block_view_alter().
 */
function spa_admin_helper_block_view_alter(array &$build, \Drupal\Core\Block\BlockPluginInterface $block) {
  // Hide the page title.
  if ($block->getBaseId() === 'page_title_block' && \Drupal::service('spa_admin_helper.embed_handler')->isEmbedding(\Drupal::request())) {
    $build['#access'] = FALSE;
  }
}

/**
 * Implements hook_entity_view_alter().
 */
function spa_admin_helper_entity_view_alter(array &$build, \Drupal\Core\Entity\EntityInterface $entity, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display) {
  $embed_handler = \Drupal::service('spa_admin_helper.embed_handler');
  if ($embed_handler->isEmbedding(\Drupal::request()) && !($embed_handler->getActiveProvider() instanceof StaticSpaProvider)) {
    unset($build['#contextual_links']);
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function spa_admin_helper_preprocess_block__lb_plus_administration_block(&$variables) {
  if (_spa_admin_helper_is_gin_related_theme()) {
    $variables['content']['lb_plus_actions']['#attributes']['class'][] = 'gin-sticky';
    $variables['content']['lb_plus_actions']['#attributes']['class'][] = 'lb-plus--invisible';
    $variables['content']['lb_plus_actions']['#attributes']['class'][] = 'lb-plus-sticky';
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Use the spa_admin page template for non-admin pages that are embedding the
 * app.
 */
function spa_admin_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  $is_embedding = \Drupal::service('spa_admin_helper.embed_handler')->isEmbedding(\Drupal::request());
  $is_admin = str_starts_with(\Drupal::service('path.current')->getPath(), '/admin/');
  if ($is_embedding && !$is_admin) {
    array_unshift($suggestions, 'page__spa_admin_embed');
  }
}

/**
 * Check if current theme is Gin or a subtheme of Gin.
 *
 * @return bool
 */
function _spa_admin_helper_is_gin_related_theme() {
  $active_theme = \Drupal::theme()->getActiveTheme();
  $base_themes = array_keys($active_theme->getBaseThemeExtensions());
  return in_array('gin', $base_themes);
}

/**
 * Implements hook_preprocess_HOOK().
 *
 * Remove the 'You have unsaved changes.' warning added by layout builder.
 * @see \Drupal\layout_builder\EventSubscriber\PrepareLayout::onPrepareLayout()
 */
function spa_admin_helper_preprocess_status_messages(&$variables) {
  $suppress = \Drupal::config('spa_admin_helper.settings')->get('suppress_lb_warnings');

  if ($suppress && !empty($variables['message_list']['warning'])) {
    foreach ($variables['message_list']['warning'] as $warning) {
      if ((string) $warning === (string) t('You have unsaved changes.')) {
        unset($variables['message_list']['warning']);
      }
    }
  }
}
